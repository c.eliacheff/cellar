import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {filter, map, mergeMap} from 'rxjs/operators';
import {AppEpic} from '../../../app/store';
import {Bookmark} from '../core/bookmark';

interface BookmarksState {
    error: string;
    bookmarks: Bookmark[];
}

export const initialState: BookmarksState = {bookmarks: [], error: ''};

// TODO: normalize data: https://redux-toolkit.js.org/usage/usage-guide#managing-normalized-data
const bookmarksSlice = createSlice({
    name: 'bookmarks',
    initialState,
    reducers: {
        loadBookmarks() {
        },
        bookmarksLoaded(state, action: PayloadAction<Bookmark[]>) {
            action.payload.map(b => state.bookmarks.push(b))
        },
        addBookmark(state, action: PayloadAction<Bookmark>) {
        },
        bookmarkAdded(state, action: PayloadAction<Bookmark>) {
            state.bookmarks.push(action.payload)
        },
        editBookmark(state, action: PayloadAction<Bookmark>) {
        },
        bookmarkEdited(state, action: PayloadAction<Bookmark>) {
            const index = state.bookmarks.findIndex((bookmark) => bookmark.id === action.payload.id);
            state.bookmarks[index].title = action.payload.title;
            state.bookmarks[index].url = action.payload.url;
        },
        removeBookmark(state, action: PayloadAction<Bookmark>) {
        },
        bookmarkRemoved(state, action: PayloadAction<Bookmark>) {
            state.bookmarks = state.bookmarks.filter((bookmark) => bookmark.id !== action.payload.id);
        },
        setError(state, action: PayloadAction<string>) {
            state.error = action.payload;
        }
    }
})

export const loadBookmarksEpic: AppEpic = (action$, state$, {bookmarksService}) => action$.pipe(
    filter(loadBookmarks.match),
    mergeMap(() => bookmarksService.all().pipe(
        map((response) => bookmarksLoaded(response.success ? response.data : []))
    )),
)

export const addBookmarksEpic: AppEpic = (action$, state$, {bookmarksService}) => action$.pipe(
    filter(addBookmark.match),
    mergeMap((action: PayloadAction<Bookmark>) => {
        return bookmarksService.add(action.payload).pipe(
            map((res) => {
                if (!res.success) {
                    return setError(res.error)
                }
                return bookmarkAdded(action.payload)
            }),
        )
    }),
)

export const editBookmarksEpic: AppEpic = (action$, state$, {bookmarksService}) => action$.pipe(
    filter(editBookmark.match),
    mergeMap((action: PayloadAction<Bookmark>) => {
        return bookmarksService.edit(action.payload).pipe(
            map((res) => {
                if (!res.success) {
                    return setError(res.error)
                }
                return bookmarkEdited(action.payload)
            })
        )
    }),
)

export const removeBookmarksEpic: AppEpic = (action$, state$, {bookmarksService}) => action$.pipe(
    filter(removeBookmark.match),
    mergeMap((action: PayloadAction<Bookmark>) => {
        return bookmarksService.remove(action.payload).pipe(
            map((res) => {
                if (!res.success) {
                    return setError(res.error)
                }
                return bookmarkRemoved(action.payload)
            })
        )
    }),
)

export const {
    loadBookmarks,
    bookmarksLoaded,
    addBookmark,
    bookmarkAdded,
    editBookmark,
    bookmarkEdited,
    removeBookmark,
    bookmarkRemoved,
    setError
} = bookmarksSlice.actions
export default bookmarksSlice.reducer
