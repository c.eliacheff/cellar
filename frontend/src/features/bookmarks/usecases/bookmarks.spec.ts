import {Store} from '@reduxjs/toolkit';
import {lastValueFrom} from "rxjs";
import {configureStoreWitDependencies} from '../../../app/store';
import {InMemoryBookmarksService} from '../adapters/infra/inMemoryBookmarksService';
import {selectBookmark, selectBookmarks, selectErrorBookmarks} from "../adapters/ui/selectors";
import {Bookmark} from '../core/bookmark';
import {ApiSuccess} from "../core/gateways/types";
import {
    addBookmark,
    editBookmark,
    loadBookmarks, removeBookmark
} from './bookmarks.slice';
import {flushPromises} from '../../../tests/utils';

describe('Bookmarks management', () => {
    let bookmarksService: InMemoryBookmarksService;
    let store: Store;
    let fixtures: Bookmark[]

    beforeEach(() => {
        bookmarksService = new InMemoryBookmarksService();
        store = configureStoreWitDependencies({bookmarksService})

        fixtures = [
            bookmarkFactory({id: '1', title: 'title 1', url: 'url 1'}),
            bookmarkFactory({id: '2', title: 'title 2', url: 'url 2'}),
            bookmarkFactory({id: '3', title: 'title 3', url: 'url 3'}),
        ];
    })

    describe('Listing bookmarks', () => {
        it('should return empty list if no bookmarks provided by source', async () => {
            store.dispatch(loadBookmarks());

            await expectStoreToBe([]);
        })

        it('should list bookmarks provided by source', async () => {
            bookmarksService.feedWith(fixtures);

            store.dispatch(loadBookmarks());

            await expectStoreToBe(fixtures);
        })
    });

    describe('Adding bookmarks', () => {
        it('add a new bookmark and save it to server', async () => {
            store.dispatch(addBookmark(fixtures[0]));

            await expectStoreToBe([fixtures[0]])
            const response = await lastValueFrom(bookmarksService.all()) as ApiSuccess<Bookmark[]>
            expect(response.success).toEqual(true);
            expect(response.data.find((b) => fixtures[0] === b)).not.toBeUndefined()
        })

        it('fail if there is a bookmark with same id', async () => {
            store.dispatch(addBookmark(fixtures[0]));
            store.dispatch(addBookmark(fixtures[0]));

            await expectStoreToBe([fixtures[0]])
            expect(selectErrorBookmarks(store.getState())).toEqual('Duplicate entry');
        })
    })

    describe('Edit bookmark', () => {
        it('should update a bookmark with new values', async () => {
            const bookmarkChanges = {id: fixtures[0].id, title: 'new title', url: 'new url'}
            store.dispatch(addBookmark(fixtures[0]));

            store.dispatch(editBookmark(bookmarkChanges));

            await expectStoreToBe([bookmarkChanges]);
        })

        it('fail if no bookmark with given id exists', async () => {
            const invalidBookmark = {id: 'zzz', title: 'new title', url: 'new url'}
            store.dispatch(addBookmark(fixtures[0]));

            store.dispatch(editBookmark(invalidBookmark));

            expect(selectErrorBookmarks(store.getState())).toEqual('Bookmark not found');
        })
    })

    describe('Remove bookmark', () => {
        it('should remove a bookmark', async () => {
            store.dispatch(addBookmark(fixtures[0]));
            store.dispatch(removeBookmark(fixtures[0]));

            await expectStoreToBe([]);
        })

        it('fail if no bookmark with given id exists', async () => {
            const invalidBookmark = {id: 'zzz', title: 'new title', url: 'new url'}

            store.dispatch(removeBookmark(invalidBookmark));

            expect(selectErrorBookmarks(store.getState())).toEqual('Bookmark not found');
        })
    })

    describe('One bookmark', () => {
        it('should have id, title, url when fetched', async () => {
            store.dispatch(addBookmark(fixtures[0]));

            const bookmark = selectBookmark(store.getState(), fixtures[0].id)
            expect(bookmark?.id).toEqual(fixtures[0].id)
            expect(bookmark?.id).toBeTruthy()
            expect(bookmark?.title).toEqual(fixtures[0].title)
            expect(bookmark?.title).toBeTruthy()
            expect(bookmark?.url).toEqual(fixtures[0].url)
            expect(bookmark?.url).toBeTruthy()
        });

        it('should fail when created with no id', async () => {
            const invalidBookmark: Bookmark = bookmarkFactory({id: undefined});

            store.dispatch(addBookmark(invalidBookmark));

            expect(selectErrorBookmarks(store.getState())).toEqual('Invalid entry');
            await expectStoreToBe([])
        });

        it('should fail when created with no title, url', async () => {
            const invalidBookmark: Bookmark = bookmarkFactory({title: undefined});

            store.dispatch(addBookmark(invalidBookmark));

            expect(selectErrorBookmarks(store.getState())).toEqual('Invalid entry');
            const bookmark = selectBookmark(store.getState(), invalidBookmark.id);
            expect(bookmark).toBeUndefined();
        });

        // it('should have a valid url', async () => {})
        // it('should have a valid title', async () => {})
    });

    const expectStoreToBe = async (expectedChange: any): Promise<void> => {
        await flushPromises();
        expect(selectBookmarks(store.getState())).toEqual(expectedChange);
    }

    const defaultBookmark = (): Bookmark => ({
        id: '1',
        title: 'title 1',
        url: 'url 1'
    })

    const bookmarkFactory = (p?: Partial<Bookmark>): Bookmark => ({
        ...defaultBookmark(),
        ...p
    })
});

export {}
