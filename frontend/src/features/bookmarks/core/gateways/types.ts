export type ApiResponse<T> = ApiSuccess<T> | ApiError

export type ApiSuccess<T> =
    {
        success: true,
        data: T extends void ? null : T
    }

export type ApiError =
    {
        success: false,
        error: string
    }

export const ApiSuccess = (data: any): ApiSuccess<typeof data> => ({
    success: true,
    data
});

export const ApiError = (errorMsg: string): ApiError => ({
    success: false,
    error: errorMsg
});
