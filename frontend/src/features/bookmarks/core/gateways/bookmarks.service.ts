import {Either} from "purify-ts";
import {Observable} from 'rxjs';
import {Bookmark} from "../bookmark";
import {ApiResponse} from "./types";

export interface BookmarksService {
    all(): Observable<ApiResponse<Bookmark[]>>;
    add(bookmark: Bookmark): Observable<ApiResponse<void>>;
    edit(bookmark: Bookmark): Observable<ApiResponse<void>>;
    remove(bookmark: Bookmark): Observable<ApiResponse<void>>;
}
