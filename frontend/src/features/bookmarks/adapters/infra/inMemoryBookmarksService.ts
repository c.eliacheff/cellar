import {Observable, of, throwError} from 'rxjs';
import {Bookmark} from '../../core/bookmark';
import {BookmarksService} from '../../core/gateways/bookmarks.service';
import {ApiError, ApiResponse, ApiSuccess} from "../../core/gateways/types";

export class InMemoryBookmarksService implements BookmarksService {
    state = new Map<string, Bookmark>();

    constructor() {
    }

    all(): Observable<ApiResponse<Bookmark[]>> {
        return of({success: true, data: Array.from(this.state.values())});
    }

    add(bookmark: Bookmark): Observable<ApiResponse<void>> {
        if (bookmark.id == null || bookmark.url == null || bookmark.title == null) {
            return of(ApiError('Invalid entry'))
        }
        if (Array.from(this.state.values()).filter(b => b.url === bookmark.url).length !== 0) {
            return of(ApiError('Duplicate entry'))
        }
        this.state.set(bookmark.id, bookmark);
        return of(ApiSuccess(null));
    }

    edit(bookmark: Bookmark): Observable<ApiResponse<void>> {
        if (!this.state.get(bookmark.id)) {
            return of(ApiError('Bookmark not found'))
        }
        this.state.set(bookmark.id, bookmark);
        return of({success: true, data: null});
    }

    remove(bookmark: Bookmark): Observable<ApiResponse<void>> {
        if (!this.state.get(bookmark.id)) {
            return of(ApiError('Bookmark not found'))
        }
        this.state.delete(bookmark.id);
        return of({success: true, data: null});
    }

    feedWith(inital_state: Bookmark[]): void {
        inital_state.map(b => this.state.set(b.id, b));
    }
}
