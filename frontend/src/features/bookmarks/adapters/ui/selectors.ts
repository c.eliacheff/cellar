import {createSelector} from "@reduxjs/toolkit";
import {RootState} from "../../../../app/store";

const selectSelf = (state: RootState) => state
const selectBookmarkId = (state: RootState, bookmarkId: string) => bookmarkId;
export const selectBookmarks = createSelector(selectSelf, (state: RootState) => state.bookmarks.bookmarks);
export const selectBookmark = createSelector([selectSelf, selectBookmarkId], (state: RootState) => state.bookmarks.bookmarks.find(b => b.id));
export const selectErrorBookmarks = createSelector(selectSelf, (state: RootState) => state.bookmarks.error);
