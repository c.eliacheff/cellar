import {createSelector, createSlice, PayloadAction} from '@reduxjs/toolkit';
import {RootState} from '../../app/store';

export const initialState: string = null!;

const errorSlice = createSlice({
    name: 'error',
    initialState,
    reducers: {
        setError(state, action: PayloadAction<string>) {
            state = action.payload
        }
    }
})

const selectSelf = (state: RootState) => state
export const selectError = createSelector(selectSelf, (state: RootState) => state.error);

export const { setError} = errorSlice.actions
export default errorSlice.reducer
