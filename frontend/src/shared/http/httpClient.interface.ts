import { Observable } from 'rxjs';

export interface HttpClient<T> {
    get(url: string, options: any): Observable<T>;
    post(url: string, body: any, options: any): Observable<void>;
    put(url: string, body: any, options: any): Observable<void>;
    patch(url: string, body: any, options: any): Observable<void>;
    delete(url: string, options: any): Observable<void>;
}