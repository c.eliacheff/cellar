import {BookmarksService} from "../features/bookmarks/core/gateways/bookmarks.service";

export interface Dependencies {
    bookmarksService: BookmarksService
}
