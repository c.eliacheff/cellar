import {combineReducers, createReducer} from '@reduxjs/toolkit';
import bookmarksSlice from '../features/bookmarks/usecases/bookmarks.slice';
import errorSlice from '../features/shared/errors.slice';

export const rootReducer = combineReducers({
    bookmarks: bookmarksSlice,
    error: errorSlice
})
