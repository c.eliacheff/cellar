import {combineEpics} from 'redux-observable';
import {
    addBookmarksEpic,
    editBookmarksEpic,
    loadBookmarksEpic,
    removeBookmarksEpic
} from '../features/bookmarks/usecases/bookmarks.slice';
import {AppEpic} from "./store";

export const rootEpic: AppEpic = combineEpics(
    loadBookmarksEpic,
    addBookmarksEpic,
    editBookmarksEpic,
    removeBookmarksEpic,
);
