import {AnyAction, configureStore, Store} from '@reduxjs/toolkit';
import {createEpicMiddleware, Epic} from 'redux-observable';
import {Dependencies} from './dependencies';
import {rootEpic} from './rootEpics';
import {rootReducer} from './rootReducer';

export const configureStoreWitDependencies = (dependencies: Dependencies): Store => {
  const epicMiddleware = createEpicMiddleware({
    dependencies: dependencies
  });

  const store = configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware({thunk: false}).concat(epicMiddleware),
  });

  epicMiddleware.run(rootEpic);

  return store
}

export type RootState = ReturnType<typeof rootReducer>;
export type AppEpic = Epic<AnyAction, AnyAction, void, Dependencies>;
