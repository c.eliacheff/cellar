import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { configureStoreWitDependencies } from './app/store';
import { Provider } from 'react-redux';
import {InMemoryBookmarksService} from './features/bookmarks/adapters/infra/inMemoryBookmarksService';
import * as serviceWorker from './serviceWorker';

const store = configureStoreWitDependencies({bookmarksService: new InMemoryBookmarksService()})

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
