## Cellar Projet

Bookmark management app, for learning DDD / TDD.
Stack:
- Frontend: Typescript, React, Redux, redux-observable ...
- Backend: Typescript, NestJS, Postgres
- Archi: TDD, Clean archi, DDD

For the objectives, roadmap and more details: [Wiki link](https://gitlab.com/c.eliacheff/cellar/-/wikis/home)
